// ---------------------------------------------------------------
// VERSION VANILLA JS --------------------------------------------
// ---------------------------------------------------------------

// let navbar = document.querySelector('.navbar');
// let burger = document.querySelector('.contain-burger');
// let nav = document.querySelector('.nav');

// let dropDownEvents = document.querySelector('.events-cities');
// let eventsTitle = document.querySelector('.section-title-events');

// let backToTop = document.querySelector('.back-to-top');

// window.addEventListener('resize', function () {
//   if (window.innerWidth >= 1200) {
//     nav.style.display = 'block';

//     navbar.classList.add('fixed');

//     navbarSections.forEach(element => {
//       element.style.padding = "42px 15px";
//     });
//   } else if (window.innerWidth <= 1200) {
//     nav.style.display = 'none';

//     navbar.classList.add('fixed');

//     navbarSections.forEach(element => {
//       element.style.padding = "15px 15px";
//     });
//   }
// })

// burger.addEventListener('click', function () {
//   if (nav.style.display == 'none') {
//     nav.style.display = 'block';
//     if (window.innerWidth <= 1200) {
//       navbar.classList.add('fixed');
//     }
//   } else {
//     nav.style.display = 'none';
//     navbar.classList.remove('fixed');
//   }
// });

// let navbarSections = document.querySelectorAll('.nav .section');

// window.onscroll = function() {scrollFunction()};

// function scrollFunction() {
//   if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
//     backToTop.style.display = "block";
//     if (window.innerWidth >= 1200) {
//       navbarSections.forEach(element => {
//         element.style.padding = "20px 15px";
//       });
//     }
//     dropDownEvents.style.top = "55px";
//     navbar.style.borderBottom = "4px solid #DBEDF8";
//   } else {
//     backToTop.style.display = "none";
//     if (window.innerWidth <= 1200) {
//       navbarSections.forEach(element => {
//         element.style.padding = "15px 15px";
//       });
//     } else {
//       navbarSections.forEach(element => {
//         element.style.padding = "42px 15px";
//       });
//     }
//     dropDownEvents.style.top = "80px";
//     navbar.style.borderBottom = "none";
//   }
// }



// ---------------------------------------------------------------
// VERSION JQUERY ------------------------------------------------
// ---------------------------------------------------------------

// Récupération des elements 
let navbar = $('.navbar');
let burger = $('.contain-burger');
let nav = $('.nav');

let navbarSections = $('.nav .section');

let backToTop = $('.back-to-top');

// Manipulation des elements

// Écouteur d'évènement sur le redimensionnement de la fenètre
// Lorsque l'utilisateur redimensionne la fenètre
$(window).on('resize', () => {
  // On vérifie si la largeur de la fenètre est supérieur à 1200px
  if ($(window).width() >= 1200) {
    // Si oui, on affiche la nav
    nav.css('display', 'block');
    // Et on ajoute le padding souhaité
    navbarSections.css('padding', '42px 15px');
  } else {
    // Si oui, on affiche pas la nav
    nav.css('display', 'none');
    // Et on ajoute le padding souhaité
    navbarSections.css('padding', '15px 15px');
  }
});

// Écouteur d'évènement sur le click sur le burger
// Lorsque l'utilisateur click sur le burger
burger.on('click', () => {
  // On vérifie si la nav n'est pas affichée
  if (nav.css('display') == 'none') {
    // Si elle ne l'est pas, on l'affiche
    nav.css('display', 'block');
    // Et si la fenètre est supérieur inférieur ou égale à 1200px 
    if ($(window).width() <= 1200) {
      // On met la navbar en position fixed
      navbar.addClass('fixed');
    }
  } else {
    // Si la nav est affichée, on ne l'affiche plus
    nav.css('display', 'none');
    // Et on retire la position fixed à la navbar
    navbar.removeClass('fixed');
  }
})

// Écouteur d'évènement sur le scroll 
// Lorsque l'utilisateur scroll sur la fenètre
$(window).on('scroll', () => {
  // On vérifier si le scroll est supérieur à 80px 
  if ($(document).scrollTop() > 80) {
    // Si oui, on affiche le back-to-top
    backToTop.css('display', 'block');
    // Et si la fenètre est supérieur à 1200px
    if ($(window).width() >= 1200) {
      // On réduit la navbar (le padding est sur les liens de la nav)
      navbarSections.css('padding', '20px 15px');
    }
    // Et on ajoute une bordure pour distinguer la navbar
    navbar.css('borderBottom', '4px solid #DBEDF8');
  } else {
    // Sinon, on n'affiche pas le back-to-top
    backToTop.css('display', 'none');
    // Et si la fenètre est inférieur ou égale à 1200px
    if ($(window).width() <= 1200) {
      // On adapte les paddings en fonction
      navbarSections.css('padding', '15px 15px');
    } else {
      // On adapte les paddings en fonction
      navbarSections.css('padding', '42px 15px');
    }
    // Et on retire la bordure 
    navbar.css('borderBottom', 'none');
  }
})